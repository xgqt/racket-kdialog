# Racket-KDialog

[Racket](https://racket-lang.org/) wrapper
for [KDialog](https://github.com/KDE/kdialog).


# Documentation

Official KDialog tutorial: "[Shell Scripting with KDE Dialogs](https://develop.kde.org/deploy/kdialog/)".
